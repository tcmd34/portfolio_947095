# portfolio_947095



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://git.thm.de/rcbr56/portfolio_5329100.git
git branch -M master
git push -uf origin master
```

## Integrate with your tools

- [ ] [Set up project integrations](https://git.thm.de/rcbr56/portfolio_5329100/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

## Projektbeschreibung 

Entwicklung einer persönlichen Portfolio-App mit Flutter und Hosting auf Google Firebase
Ziel des Projektes ist es eine persönliche Portfolio-App mit Flutter zu entwickeln. Diese App wird als digitales Portfolio dienen, das man potenziellen Arbeitgebern oder Kunden vorstellen kann. Am Ende des Kurses wird die App auf Google Firebase gehostet, um sie der Öffentlichkeit zugänglich zu machen.


## Student/in
 Name: Teodora Claudia Modroi
 Matrikelnummer: 947095
 E:Mail: teodora.modroi@mnd.thm.de

## Projektziele
Erlernen der Grundlagen von Flutter und Dart.
Entwicklung einer benutzerfreundlichen mobilen App mit ansprechendem Design.
Anwendung von State Management, Netzwerkkommunikation und Datenpersistenz in Flutter.
Durchführung von Tests zur Sicherstellung der App-Qualität.
Hosting der fertigen App auf Google Firebase.
Aufgaben
Startseite
Gestaltet eine Startseite, die einen Willkommensgruß, euer Foto und eine kurze Einführung enthält. Die Seite sollte intuitiv und einladend sein und zum Weiterlesen anregen.

## Eingesetzte Projekt-Tools
 
  - Google Firebase
  - GitLab 
  - Flutter (Open-Source-UI-Framework)
  - Dart (als Programmiersprache)
  - Xcode 
  - Android Studio (für Android-Entwicklung)
  - Homebrew (für die Installation von Flutter auf macOS)
  - Visual Studio Code  (IDE)

  ## Beschreibung der einzelenen Tools

  # GitLab 
        - Git: Ist ein Open-Source-Tool welches für die Versionskontrolle von Software verwendet wird.
    - GitLab:  Dient als Repository Manager. Allerdings kann GitLab selbst gehostet werden und erlaubt somit vollständige Kontrolle   
      über die eigenen Daten. GitLab wird vor allem von Teams verwendet die Projektaufgaben verwalten und ausführen wollen, da es ein Issue-Tracking-System sowie ein System für Continuous Integration und Continuous Delivery (CI/CD) beinhaltet.
 # Google FireBase
    - Firebase ist eine Entwicklungs-Plattform für mobile und Webanwendungen. Sie stellt über ein Software Development Kit Tools und
      Infrastruktur zur Verfügung, die es einem Entwickler ermöglichen sollen, einfacher und effizienter Funktionen mittels Programmierschnittstellen auf verschiedenen Plattformen bereitzustellen.
 # Flutter
    - Flutter ist ein SDK für mobile Apps, mit dem Entwickler hochwertige native Apps für iOS und Android erstellen können. Es ist 
      kostenlos und quelloffen und wurde von Google entwickelt. Dabei wird die Programmiersprache Dart genutzt, die ebenfalls von Google ist.
 # XCode
    - Xcode ist eine integrierte Entwicklungsumgebung, kurz IDE, die speziell für das Schreiben von Programmen für diverse 
      Apple-Betriebssysteme existiert.
    - Hauptsächlich richtet sich Xcode an Entwickler, die mit Objective-C oder dem moderneren Swift Programme für die genannten 
      Betriebssysteme schreiben möchten. Gleichzeitig werden jedoch auch allgemeinere Sprachen wie C und C++ sowie beispielsweise JavaScript und Ruby unterstützt.
# Android Studio
    - Android Studio ist die offizielle Entwicklungsumgebung für die Entwicklung von Android Anwendungen. Die IDE (Integrated 
      Development Environment) basiert auf der IntelliJ IDEA Community Edition Entwicklungsumgebung und verfügt somit über einen sehr mächtigen Code Editor und viele sehr hilfreiche Entwicklungs-Werkzeuge.
# Homebrew
    - Homebrew ist ein Paketmanager für die Kommandozeile zur Installation von Open-Source Anwendungen auf macOS. Linux Benutzer
      kennen diese Paketmanager, aber auf macOS fehlen diese Standards leider nativ.
# Visual Studio Code
    - Die Visual Studio IDE ist eine kreatives Startrampe, mit der Sie Code bearbeiten, debuggen und erstellen und dann eine App 
      veröffentlichen können. Über den Standard-Editor und -Debugger hinaus, welchen die meisten IDEs bieten, umfasst Visual Studio Compiler, Codeabschlusstools, grafische Designer und viele weitere Funktionen zur Verbesserung des Softwareentwicklungsprozesses.


